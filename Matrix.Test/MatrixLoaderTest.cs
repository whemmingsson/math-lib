﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Matrix.Test
{
    [TestClass]
    public class MatrixLoaderTest
    {
        [TestMethod]
        public void LoadMatrixSuccessTest()
        {
            var path = "C:/temp/test/matrix.txt";

            var loader = new MatrixFileLoader<int>();

            var result = loader.Load(path);

            Assert.AreEqual(2, result.Rows);
            Assert.AreEqual(2, result.Columns);
        }

        [TestMethod]
        public void LoadMatrixFailTest()
        {
            var path = "C:/temp/test/matrix1.txt";
            var loader = new MatrixFileLoader<int>();
            try
            {
                var result = loader.Load(path);
            }
            catch (Exception e)
            {
                Assert.IsTrue(true);
                return;
            }
            
            Assert.IsTrue(false);         
        }

        [TestMethod]
        public void LoadMatrixFail2Test()
        {
            var path = "C:/temp/test/matrix2.txt";
            var loader = new MatrixFileLoader<int>();
            try
            {
                var result = loader.Load(path);
            }
            catch (Exception e)
            {
                Assert.IsTrue(true);
                return;
            }

            Assert.IsTrue(false);
        }
    }
}
