﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Matrix.Test
{
    [TestClass]
    public class MatrixTest
    {
        private Matrix<int> _matrix;
            
        [TestInitialize]
        public void Init()
        {
            _matrix = new Matrix<int>(2,2);
        }

        public void CreateNewMatrix(int width, int height)
        {
            _matrix = new Matrix<int>(width, height);
        }

        [TestMethod]
        public void CreateMatrixTest()
        {
            var size = 2;
            var matrix = new Matrix<int>(size, size);

            Assert.AreEqual(size, matrix.Rows);
            Assert.AreEqual(size, matrix.Columns);
        }

        [TestMethod]
        public void CreateAndFillMatrixTest()
        {
            var size = 2;
            int value = 5;
            var matrix = new Matrix<int>(size, size);
            matrix.Initilize(value);

            Assert.AreEqual(value, matrix.Get(0, 0));
            Assert.AreEqual(value, matrix.Get(0, 1));
            Assert.AreEqual(value, matrix.Get(1, 0));
            Assert.AreEqual(value, matrix.Get(1, 1));
        }

        [TestMethod]
        public void SetValueTest()
        {
            int value = 5;
            _matrix.Set(0,0,value);

            Assert.AreEqual(value, _matrix.Get(0,0));
        }

        [TestMethod]
        public void SetValueOutOfBoundsTest()
        {
            try
            {
                int value = 5;
                _matrix.Set(5, 5, value);
            }
            catch (Exception e)
            {
                Assert.IsTrue(e is IndexOutOfRangeException);
            }           
        }

        [TestMethod]
        public void GetValueOutOfBoundsTest()
        {
            try
            {
                _matrix.Get(5, 5);
            }
            catch (Exception e)
            {
                Assert.IsTrue(e is IndexOutOfRangeException);
            }
        }
        
        [TestMethod]
        public void ToStringTest()
        {
            _matrix.Initilize(7);
            Assert.AreEqual("7 7\n7 7", _matrix.ToString());                 
        }

        [TestMethod]
        public void GetRowLengthTest()
        {
            _matrix.Set(0,0, 1);
            _matrix.Set(1,0, 2);

            var row = _matrix.GetRow(0);
            
            Assert.AreEqual(2, row.Length);
        }

        [TestMethod]
        public void GetRowValueTest()
        {
            _matrix.Set(0, 0, 1);
            _matrix.Set(1, 0, 2);

            var row = _matrix.GetRow(0);

            Assert.AreEqual(1, row[0]);
            Assert.AreEqual(2, row[1]);
        }

        [TestMethod]
        public void GetColLenghtTest()
        {
            _matrix.Set(0, 0, 1);
            _matrix.Set(0, 1, 2);

            var col = _matrix.GetColumn(0);

            Assert.AreEqual(2, col.Length);
        }

        [TestMethod]
        public void GetColValueTest()
        {
            _matrix.Set(0, 0, 1);
            _matrix.Set(0, 1, 2);

            var col = _matrix.GetColumn(0);

            Assert.AreEqual(1, col[0]);
            Assert.AreEqual(2, col[1]);
        }

        [TestMethod]
        public void GetSubMatrixSizeTest()
        {
            var value = 3;
            CreateNewMatrix(9,9);
            _matrix.Initilize(value); // Call this to get real values
            var subMatrix = _matrix.GetSubMatrix(0, 0, 3, 3);

            Assert.AreEqual(3, subMatrix.Columns);
            Assert.AreEqual(3, subMatrix.Rows);
        }

        [TestMethod]
        public void GetSubMatrixValueTest()
        {
            CreateNewMatrix(4, 4);
            _matrix.Initilize(0);
            _matrix.Set(0, 0, 1);
            _matrix.Set(1, 0, 2);
            _matrix.Set(0, 1, 3);
            _matrix.Set(1, 1, 4);

            var subMatrix = _matrix.GetSubMatrix(0, 0, 2, 2);

            Assert.AreEqual("1 2\n3 4", subMatrix.ToString());
        }

        [TestMethod]
        public void CloneTest()
        {
            CreateNewMatrix(4, 4);
            _matrix.Initilize(5);

            var clone = _matrix.Clone();

            Assert.IsTrue(_matrix.ToString().Equals(clone.ToString()));
        }

        [TestMethod]
        public void SplitYLengthTest()
        {
            CreateNewMatrix(4,4);

            var halfs = _matrix.SplitHorizontally(2);

            Assert.AreEqual(2, halfs.Length);
        }

        [TestMethod]
        public void SplitYSubMatrixSizeTest()
        {
            CreateNewMatrix(4, 4);

            var halfs = _matrix.SplitHorizontally(2);

            Assert.AreEqual(2, halfs[0].Columns);
            Assert.AreEqual(4, halfs[0].Rows);
            Assert.AreEqual(2, halfs[1].Columns);
            Assert.AreEqual(4, halfs[1].Rows);
        }

        [TestMethod]
        public void SplitYSubMatrixValueTest()
        {
            CreateNewMatrix(2, 2);

            _matrix.Set(0,0,1);
            _matrix.Set(1,0,2);
            _matrix.Set(0,1,3);
            _matrix.Set(1,1,4);

            var halfs = _matrix.SplitHorizontally(1);

            Assert.AreEqual("1\n3", halfs[0].ToString());
            Assert.AreEqual("2\n4", halfs[1].ToString());
        }

        [TestMethod]
        public void ConcatenateTest()
        {
            var matrix1 = new Matrix<int>(2, 2);
            matrix1.Initilize(1);

            var matrix2 = new Matrix<int>(2, 2);
            matrix2.Initilize(2);

            var bigMatrix = Matrix.ConcatenateHorizontally(new[] {matrix1, matrix2});

            Assert.AreEqual(2, bigMatrix.Rows);
            Assert.AreEqual(matrix1.Columns + matrix2.Columns, bigMatrix.Columns);
            Assert.AreEqual("1 1 2 2\n1 1 2 2", bigMatrix.ToString());
        }

        [TestMethod]
        public void ConcatenateSmallTest()
        {
            var matrix1 = new Matrix<int>(1, 2);
            matrix1.Initilize(1);

            var matrix2 = new Matrix<int>(1, 2);
            matrix2.Initilize(2);

            var bigMatrix = Matrix.ConcatenateHorizontally(new[] { matrix1, matrix2 });

            Assert.AreEqual("1 2\n1 2", bigMatrix.ToString());
        }

        [TestMethod]
        public void ConcatenateVerySmallTest()
        {
            var matrix1 = new Matrix<int>(1, 1);
            matrix1.Initilize(1);

            var matrix2 = new Matrix<int>(1, 1);
            matrix2.Initilize(2);

            var bigMatrix = Matrix.ConcatenateHorizontally(new[] { matrix1, matrix2 });

            Assert.AreEqual("1 2", bigMatrix.ToString());
        }

        [TestMethod]
        public void InsertSubMatrixYTest()
        {
            CreateNewMatrix(4,2);
            _matrix.Initilize(4);

            var subMatrix = new Matrix<int>(2, 2);
            subMatrix.Initilize(2);

            _matrix.InsertSubMatrixHorizontally(subMatrix, 2);

            Assert.AreEqual("4 4 2 2 4 4\n4 4 2 2 4 4", _matrix.ToString());
        }

        [TestMethod]
        public void InsertColumnTest()
        {
            _matrix.Initilize(1);

            var column = new int[] {2, 2};

            _matrix.InsertColumn(column, 1);

            Assert.AreEqual("1 2 1\n1 2 1", _matrix.ToString());
        }

        [TestMethod]
        public void InsertRowTest()
        {
            _matrix.Initilize(1);

            var row = new int[] { 2, 2 };

            _matrix.InsertRow(row, 1);

            Assert.AreEqual("1 1\n2 2\n1 1", _matrix.ToString());
        }


        [TestMethod]
        public void SplitVerticallySizeTest()
        {
            CreateNewMatrix(4, 8);
            _matrix.Initilize(4);

            var halfs = _matrix.SplitVertically(4);

            Assert.AreEqual(4, halfs[0].Columns);
            Assert.AreEqual(4, halfs[0].Rows);
            Assert.AreEqual(4, halfs[1].Columns);
            Assert.AreEqual(4, halfs[1].Rows);
        }

        [TestMethod]
        public void SplitVerticallyValueTest()
        {
            _matrix.Initilize(1);
            _matrix.Set(0, 1, 2);
            _matrix.Set(1, 1, 2);

            var halfs = _matrix.SplitVertically(1);

            Assert.AreEqual(1, halfs[0].Get(0,0));
            Assert.AreEqual(1, halfs[0].Get(1,0));

            Assert.AreEqual(2, halfs[1].Get(0,0));
            Assert.AreEqual(2, halfs[1].Get(1,0));
        }

        [TestMethod]
        public void ConcatenateVerticallyTest()
        {
            var matrix1 = new Matrix<int>(2, 2);
            matrix1.Initilize(1);

            var matrix2 = new Matrix<int>(2, 2);
            matrix2.Initilize(2);

            var result = Matrix.ConcatenateVertically(new Matrix<int>[] {matrix1, matrix2});

            Assert.AreEqual("1 1\n1 1\n2 2\n2 2", result.ToString());
        }

        [TestMethod]
        public void InsertSubMatrixVerticallyTest()
        {
            _matrix.Initilize(2);
            var subMatrix = new Matrix<int>(2, 1);
            subMatrix.Initilize(5);

            _matrix.InsertSubMatrixVertically(subMatrix, 1);
          
            Assert.AreEqual("2 2\n5 5\n2 2", _matrix.ToString());
        }

        [TestMethod]
        public void InsertHorizontallyEndTest() // Also known as "Append to end"
        {
            _matrix.Initilize(1);
            var subMatrix = new Matrix<int>(2, 2);
            subMatrix.Initilize(2);

            _matrix.InsertSubMatrixHorizontally(subMatrix, 2);

            Assert.AreEqual(4, _matrix.Columns);
            Assert.AreEqual(2, _matrix.Rows);
            Assert.AreEqual("1 1 2 2\n1 1 2 2", _matrix.ToString());
        }

        [TestMethod]
        public void InsertHorizontallyBegnningTest() // Also known as "Append to beginning"
        {
            _matrix.Initilize(1);
            var subMatrix = new Matrix<int>(2, 2);
            subMatrix.Initilize(2);

            _matrix.InsertSubMatrixHorizontally(subMatrix, 0);
            Assert.AreEqual("2 2 1 1\n2 2 1 1", _matrix.ToString());           
        }


        [TestMethod]
        public void AddIntMatrixTest()
        {
            var m1 = new Matrix<int>(2, 2);
            m1.Initilize(3);

            var m2 = new Matrix<int>(2, 2);
            m2.Initilize(4);

            var result = m1 + m2;

            Assert.AreEqual("7 7\n7 7", result.ToString());
        }

        [TestMethod]
        public void AddDoubleMatrixTest()
        {
            var m1 = new Matrix<double>(2, 2);
            m1.Initilize(2.5);

            var m2 = new Matrix<double>(2, 2);
            m2.Initilize(5.2);

            var result = m1 + m2;

            Assert.AreEqual("7,7 7,7\n7,7 7,7", result.ToString());
        }

        [TestMethod]
        public void AddStringMatrixTest()
        {
            var m1 = new Matrix<string>(2, 2);
            m1.Initilize("a");

            var m2 = new Matrix<string>(2, 2);
            m2.Initilize("b");

            var result = m1 + m2;

            Assert.AreEqual("ab ab\nab ab", result.ToString());
        }

        [TestMethod]
        public void AddBoolMatrixTest()
        {
            var m1 = new Matrix<bool>(2, 2);
            m1.Initilize(true);

            var m2 = new Matrix<bool>(2, 2);
            m2.Initilize(false);

            try
            {
                var result = m1 + m2;
            }
            catch (Exception e)
            {
                Assert.IsTrue(e is InvalidOperationException);
            }                       
        }

        [TestMethod]
        public void SubstractIntMatrixTest()
        {
            var m1 = new Matrix<int>(2, 2);
            m1.Initilize(10);

            var m2 = new Matrix<int>(2, 2);
            m2.Initilize(5);

            var result = m1 - m2;

            Assert.AreEqual("5 5\n5 5", result.ToString());
        }

        [TestMethod]
        public void MultiplyScalarIntMatrixTest()
        {
            _matrix.Initilize(5);

            var result = _matrix*2;

            Assert.AreEqual("10 10\n10 10", result.ToString());
        }

        [TestMethod]
        public void MultiplyScalarIntMatrix2Test()
        {
            _matrix.Initilize(5);

            var result = 2*_matrix;

            Assert.AreEqual("10 10\n10 10", result.ToString());
        }

        //[TestMethod]
        //public void MultiplyIntTest()
        //{
        //   // _matrix

        //    var result = 2 * _matrix;

        //    Assert.AreEqual("10 10\n10 10", result.ToString());
        //}


        [TestMethod]
        public void ExistsSimpleTest()
        {
            _matrix.Initilize(1);
            Assert.IsTrue(_matrix.Exists(1));          
        }

        [TestMethod]
        public void ExistsComplexTest()
        {
            _matrix.Initilize(5);
            Assert.IsTrue(_matrix.Exists(x=>x < 10));
        }

        [TestMethod]
        public void DontExistsSimpleTest()
        {
            _matrix.Initilize(5);
            Assert.IsFalse(_matrix.Exists(1));
        }

        [TestMethod]
        public void DontExistsComplexTest()
        {
            _matrix.Initilize(5);
            Assert.IsFalse(_matrix.Exists(x=>x > 5));
        }

        [TestMethod]
        public void ExistsInRowTest()
        {
            _matrix.Initilize(1);
            _matrix.Set(0, 1, 2);
            _matrix.Set(1, 1, 2);
            Assert.IsFalse(_matrix.ExistsInRow(1, 1));
        }

        [TestMethod]
        public void RotateSizeTest()
        {
            _matrix.Initilize(2);
            _matrix.Set(1,0, 2);
            _matrix.Set(1, 1, 2);

            _matrix.Rotate(Matrix.Degrees.Clockwise90);

            Assert.AreEqual(2, _matrix.Rows);
            Assert.AreEqual(2, _matrix.Columns);

        }

        [TestMethod]
        public void RotateValueTest()
        {
            _matrix.Initilize(1);
            _matrix.Set(0, 1, 2);
            _matrix.Set(1, 1, 2);

             _matrix.Rotate(Matrix.Degrees.Clockwise90);

            Assert.AreEqual(2, _matrix.Get(0,0));
            Assert.AreEqual(2, _matrix.Get(0, 1));
        }

        [TestMethod]
        public void Rotate180ValueTest()
        {
            _matrix.Initilize(1);
            _matrix.Set(0, 1, 2);
            _matrix.Set(1, 1, 2);

            _matrix.Rotate(Matrix.Degrees.Clockwise180);

            Assert.AreEqual(2, _matrix.Get(0, 0));
            Assert.AreEqual(2, _matrix.Get(1, 0));
        }

        [TestMethod]
        public void MultiplyTest()
        {
            var m1 = new Matrix<int>(2, 2);
            m1.Initilize(2);

            var m2 = new Matrix<int>(2, 2);
            m2.Initilize(4);

            var result = m1 * m2;

            Assert.AreEqual(16, result.Get(0, 0));
            Assert.AreEqual(16, result.Get(1, 0));
            Assert.AreEqual(16, result.Get(0, 1));
            Assert.AreEqual(16, result.Get(1, 1));
        }

        [TestMethod]
        public void Multiply2Test()
        {
            var m1 = new Matrix<int>(3, 2);
            m1.Set(0, 0, 1);
            m1.Set(1, 0, 2);
            m1.Set(2, 0, 3);
            m1.Set(0, 1, 4);
            m1.Set(1, 1, 5);
            m1.Set(2, 1, 6);

            var m2 = new Matrix<int>(2, 3);
            m2.Set(0, 0, 1);
            m2.Set(1, 0, 2);
            m2.Set(0, 1, 3);
            m2.Set(1, 1, 4);
            m2.Set(0, 2, 5);
            m2.Set(1, 2, 6);

           
            var result = m1 * m2;

            Assert.AreEqual(22, result.Get(0, 0));
            Assert.AreEqual(28, result.Get(1, 0));
            Assert.AreEqual(49, result.Get(0, 1));
            Assert.AreEqual(64, result.Get(1, 1));         
        }

        [TestMethod]
        public void OneByOneTest()
        {
            CreateNewMatrix(1,1);
            _matrix.Initilize(2);

            Assert.AreEqual(1, _matrix.Rows);
            Assert.AreEqual(1, _matrix.Columns);
            Assert.AreEqual(2, _matrix.Get(0,0));

        }

        [TestMethod]
        public void DeterminantBaseCaseTest()
        {
            _matrix.Initilize(2);

            var result = _matrix.GetDeterminant();

            Assert.AreEqual(0, result);
        }

        [TestMethod]
        public void DeterminantSimpleCaseTest()
        {
            CreateNewMatrix(3,3);

            _matrix.Set(0, 0, 1);
            _matrix.Set(1, 0, 2);
            _matrix.Set(2, 0, 3);
            _matrix.Set(0, 1, -3);
            _matrix.Set(1, 1, -2);
            _matrix.Set(2, 1, -1);
            _matrix.Set(0, 2, 0);
            _matrix.Set(1, 2, 0);
            _matrix.Set(2, 2, 5);

            var result = _matrix.GetDeterminant();

            Assert.AreEqual(20, result);
        }
    }
}
