﻿using System;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Matrix.Test
{
    [TestClass]
    public class MatrixPerformanceTest
    {
        [TestMethod]
        public void DeterminantPerformanceTest()
        {
            Random r = new Random();
            Stopwatch watch = new Stopwatch();
            string result = "";
            Matrix<int> matrix;

            for (int i = 2; i < 15; i ++)
            {
                matrix = new Matrix<int>(i);

                for (int x = 0; x < i; x++)
                {
                    for (int y = 0; y < i; y++)
                    {
                        matrix.Set(x, y, r.Next(-10,11));
                    }
                }
                    matrix.Initilize(1);

                watch.Start();
                matrix.GetDeterminant();
                watch.Stop();

                result += i + " " + watch.ElapsedMilliseconds + "\n";

                watch.Reset();
            }

            Assert.IsTrue(true);
        }
    }
}
