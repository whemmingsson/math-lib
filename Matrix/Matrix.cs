﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Matrix
{
    /// <summary>
    /// Base class for matrix
    /// </summary>
    /// <typeparam name="T">Value type for matrix</typeparam>
    public class Matrix<T> where T : IConvertible
    {
        #region [Fields and properties]

        private T[][] _matrix;

        /// <summary>
        /// Width of the matrix
        /// </summary>
        public int Columns { private set; get; }

        /// <summary>
        /// Height of the matrix
        /// </summary>
        public int Rows { private set; get; }

        /// <summary>
        /// Returns the count (number of elements) of the matrix as a number
        /// </summary>
        public int Count
        {
            get { return Rows * Columns; }
        }

        /// <summary>
        /// Returns true if the matrix is a square; false if not
        /// </summary>
        public bool IsSquare
        {
            get { return Rows == Columns; }
        }

        /// <summary>
        /// Returns the size of the matrix in m x n notation
        /// </summary>
        public string Size
        {
            get { return Rows + "x" + Columns; }
        }

        #endregion

        #region [Constructors]

        /// <summary>
        /// Contructs a matrix of the provided size
        /// </summary>
        /// <param name="columns">Number of columns is the new matrix</param>
        /// <param name="rows">Number of rows in the new matrix</param>
        public Matrix(int columns, int rows)
        {
            Columns = columns;
            Rows = rows;
            AllocateMemory();
        }

        /// <summary>
        /// Contructs a square matrix of the provided size
        /// </summary>
        /// <param name="size">The number of rows and columns in the new matrix</param>
        public Matrix(int size)
        {
            Columns = size;
            Rows = size;
            AllocateMemory();
        }

        #endregion

        #region [Setup and initialization]
        /// <summary>
        /// Allocates the memory needed for the matrix. Overwrites the current matrix if present.
        /// </summary>
        private void AllocateMemory()
        {
            _matrix = new T[Columns][];
            for (var i = 0; i < Columns; i++) { _matrix[i] = new T[Rows]; }
        }

        /// <summary>
        /// Creates the matrix from the provided row
        /// </summary>
        /// <param name="row">Row used in matrix construction</param>
        public void SetupFromRow(T[] row)
        {
            Rows = 1;
            Columns = row.Length;
            AllocateMemory();

            for (var j = 0; j < Columns; j++)
            {
                _matrix[j][0] = row[j];
            }
        }

        /// <summary>
        /// Creates the matrix from the provided column
        /// </summary>
        /// <param name="column">Column used in matrix construction</param>
        public void SetupFromColumn(T[] column)
        {
            Rows = column.Length;
            Columns = 1;
            AllocateMemory();

            _matrix[0] = column;
        }

        /// <summary>
        /// Fills the matrix with the provided value
        /// </summary>
        /// <param name="value">Fills the matrix with this value</param>
        public void Initilize(T value)
        {
            for (var i = 0; i < Rows; i++)
            {
                for (var j = 0; j < Columns; j++)
                {
                    Set(j, i, value);
                }
            }
        }
        #endregion

        #region [Basic structural operations]

        /// <summary>
        /// Get a value from the matrix
        /// </summary>
        /// <param name="column">Column number</param>
        /// <param name="row">Row number</param>
        /// <exception cref="System.IndexOutOfRangeException">Thrown when trying to access an element not inside the bounds of the matrix</exception>
        /// <returns>Value of type T</returns>
        public T Get(int column, int row)
        {
            return _matrix[column][row];
        }

        /// <summary>
        /// Set a value in the matrix
        /// </summary>
        /// <param name="column">Column number</param>
        /// <param name="row">Row number</param>
        /// <param name="value">Value to set</param>
        /// <exception cref="System.IndexOutOfRangeException">Thrown when trying to set an element not inside the bounds of the matrix</exception>
        public void Set(int column, int row, T value)
        {
            _matrix[column][row] = value;
        }

        /// <summary>
        /// Get a column (clone) from the matrix
        /// </summary>
        /// <param name="column">Column number</param>
        /// <exception cref="System.IndexOutOfRangeException">Thrown when trying to access a column not inside the bounds of the matrix</exception>
        /// <returns>A copy of the column with the provided column number</returns>
        public T[] GetColumn(int column)
        {
            return (T[])_matrix[column].Clone();
        }

        /// <summary>
        /// Get a row (clone) from the matrix
        /// </summary>
        /// <param name="row">Row number</param>
        /// <exception cref="System.IndexOutOfRangeException">Thrown when trying to access a row not inside the bounds of the matrix</exception>
        /// <returns>A copy of the row with the provided column number</returns>
        public T[] GetRow(int row)
        {
            var result = new T[Columns];

            for (var i = 0; i < Columns; i++)
            {
                result[i] = Get(i, row);
            }

            return result;
        }
        #endregion

        #region [Advanced structural operations]
        /// <summary>
        /// Extracts a sub matrix
        /// </summary>
        /// <param name="column">Column number</param>
        /// <param name="row">Row number</param>
        /// <param name="width">Width of new matrix</param>
        /// <param name="height">Height of new matrix</param>
        /// <exception cref="System.IndexOutOfRangeException">Thrown in several scenarios involving the access of/reading from positions not present in the current matrix</exception>
        /// <returns>A new matrix from the orginal based on input parameters</returns>
        public Matrix<T> GetSubMatrix(int column, int row, int width, int height)
        {
            var result = new Matrix<T>(width, height);
            var iNew = 0;
            for (var i = 0; i < height; i++)
            {
                var jNew = 0;

                for (var j = 0; j < width; j++)
                {
                    result.Set(jNew, iNew, Get(j + column, i + row));
                    jNew++;
                }

                iNew++;
            }
            return result;
        }

        /// <summary>
        /// Extracts a sub matrix that is defined be the bounding parameters width and height. Matrix width and height must be dividable by the bounding box parameters.
        /// </summary>
        /// <param name="column">Column number</param>
        /// <param name="row">Row number</param>
        /// <param name="width">Width of new matrix and also the width of the bounding box</param>
        /// <param name="height">Height of new matrix and also the height of the bounding box</param>
        /// <exception cref="System.IndexOutOfRangeException">Thrown in several scenarios involving the access of/reading from positions not present in the current matrix</exception>
        /// <returns>A new matrix from the orginal based on input parameters</returns>
        public Matrix<T> GetSubMatrixBounded(int column, int row, int width, int height)
        {
            // Calculate the parameters for the new matrix based on the bounding box
            if (Columns%width != 0 ||  Rows%height != 0)
            {
                throw new ArgumentException("Bounding parameters are invalid");
            }

            int subMatrixCol = (column  / width) * width;
            int subMatrixRow = (row / height) * height;

            return GetSubMatrix(subMatrixCol, subMatrixRow, width, height);
        }

        /// <summary>
        /// Creates a clone of the matrix
        /// </summary>
        /// <returns>Matrix with same size and content as the current matrix</returns>
        public Matrix<T> Clone()
        {
            return GetSubMatrix(0, 0, Columns, Rows);
        }

        /// <summary>
        /// Inserts a column at the provided columnPosition
        /// </summary>
        /// <param name="column">New column</param>
        /// <param name="columnPosition">Position to insert</param>
        public void InsertColumn(T[] column, int columnPosition)
        {
            var middleMatrix = new Matrix<T>(0, 0);
            middleMatrix.SetupFromColumn(column);
            InsertSubMatrixHorizontally(middleMatrix, columnPosition);
        }

        /// <summary>
        /// Inserts a row at the provided rowPosition
        /// </summary>
        /// <param name="row">New row</param>
        /// <param name="rowPosition">Position to insert</param>
        public void InsertRow(T[] row, int rowPosition)
        {
            var middleMatrix = new Matrix<T>(0, 0);
            middleMatrix.SetupFromRow(row);
            InsertSubMatrixVertically(middleMatrix, rowPosition);
        }

        /// <summary>
        /// Inserts a sub matrix horizontally. Numbers of rows must match.
        /// </summary>
        /// <param name="matrix">New matrix</param>
        /// <param name="columnPosition">Position to insert on</param>
        public void InsertSubMatrixHorizontally(Matrix<T> matrix, int columnPosition)
        {
            var subMatrices = SplitHorizontally(columnPosition);
            var result = Matrix.ConcatenateHorizontally(new[] { subMatrices[0], matrix, subMatrices[1] });
            SetMatrix(result);
        }

        /// <summary>
        /// Inserts a sub matrix vertically. Numbers of columns must match.
        /// </summary>
        /// <param name="matrix">New matrix</param>
        /// <param name="columnPosition">Position to insert on</param>
        public void InsertSubMatrixVertically(Matrix<T> matrix, int columnPosition)
        {
            var subMatrices = SplitVertically(columnPosition);
            var result = Matrix.ConcatenateVertically(new[] { subMatrices[0], matrix, subMatrices[1] });
            SetMatrix(result);
        }

        /// <summary>
        /// Splits the matrix horizontally into an array of two matrices
        /// </summary>
        /// <param name="columnPosition">Split the matrix in this column. The column ends up in the first matrix in the result.</param>
        /// <returns>Array of two matrices</returns>
        public Matrix<T>[] SplitHorizontally(int columnPosition)
        {
            var leftMatrix = GetSubMatrix(0, 0, columnPosition, Rows);
            var rightMatrix = GetSubMatrix(columnPosition, 0, Columns - columnPosition, Rows);
            return new[] { leftMatrix, rightMatrix };
        }

        /// <summary>
        /// Splits the matrix vertically into an array of two matrices
        /// </summary>
        /// <param name="rowPosition">Split the matrix in this row. The row ends up in the first matrix in the result.</param>
        /// <returns>Array of two matrices</returns>
        public Matrix<T>[] SplitVertically(int rowPosition)
        {
            var topMatrix = GetSubMatrix(0, 0, Columns, rowPosition);
            var bottomMatrix = GetSubMatrix(0, rowPosition, Columns, Rows - rowPosition);
            return new[] { topMatrix, bottomMatrix };
        }

        /// <summary>
        /// Rotates the matrix
        /// </summary>
        /// <param name="degrees">Number of degrees to rotate the matrix</param>
        public void Rotate(Matrix.Degrees degrees)
        {
            var matrix = Matrix.Rotate(this);
            switch (degrees)
            {
                case Matrix.Degrees.Clockwise90:
                    {
                        SetMatrix(matrix);
                    }
                    break;
                case Matrix.Degrees.Clockwise180:
                    {
                        matrix = Matrix.Rotate(matrix);
                        SetMatrix(matrix);
                    }
                    break;
                default:
                    {
                        matrix = Matrix.Rotate(matrix);
                        matrix = Matrix.Rotate(matrix);
                        SetMatrix(matrix);
                    }
                    break;
            }
        }

        #endregion

        #region [Mathematical operations]

        /// <summary>
        /// Addes to matrices together
        /// </summary>
        /// <param name="m1">First matrix</param>
        /// <param name="m2">Second matrix</param>
        /// <returns>A matrix where element values have been added</returns>
        public static Matrix<T> operator +(Matrix<T> m1, Matrix<T> m2)
        {
            return Matrix.DoOperation<T>(m1, m2, (x, y) => x + y);
        }

        /// <summary>
        /// Substracts matrices, m1 minus m2
        /// </summary>
        /// <param name="m1">First matrix</param>
        /// <param name="m2">Second matrix</param>
        /// <returns>A matrix where element values have been substracted</returns>
        public static Matrix<T> operator -(Matrix<T> m1, Matrix<T> m2)
        {
            return Matrix.DoOperation<T>(m1, m2, (x, y) => x - y);
        }

        /// <summary>
        ///  Multiplies every element value in the matrix with a scalar valye
        /// </summary>
        /// <param name="m1">Matrix</param>
        /// <param name="value">Scalar value of type T</param>
        /// <returns>A matrix where element values have been multiplied with provied scalar</returns>
        public static Matrix<T> operator *(Matrix<T> m1, T value)
        {
            var m2 = new Matrix<T>(m1.Columns, m1.Rows);
            m2.Initilize(value);
            return Matrix.DoOperation<T>(m1, m2, (x, y) => x * y);
        }

        /// <summary>
        ///  Multiplies every element value in the matrix with a scalar valye
        /// </summary>
        /// <param name="m2">Matrix</param>
        /// <param name="value">Scalar value of type T</param>
        /// <returns>A matrix where element values have been multiplied with provied scalar</returns>
        public static Matrix<T> operator *(T value, Matrix<T> m2)
        {
            return m2 * value;
        }

        /// <summary>
        /// Multiply the two matices with eachother. T can only be integer or double.
        /// </summary>
        /// <param name="m1">First matrix</param>
        /// <param name="m2">Second matrix</param>
        /// <returns>A matrix with type double</returns>
        public static Matrix<double> operator *(Matrix<T> m1, Matrix<T> m2)
        {
            if (typeof(T) != typeof(int) && typeof(T) != typeof(double))
            {
                throw new InvalidOperationException("Multiplying matrices where T is not Integer or Double is not possible");
            }

            if (m1.Columns != m2.Rows)
            {
                throw new ArgumentException("Number of columns in the first matrix must match the rows in the second matrix");
            }

            var result = new Matrix<double>(m2.Columns, m1.Rows);

            for (var i = 0; i < m1.Rows; i++)
            {
                var row = m1.GetRow(i);

                for (var j = 0; j < m2.Columns; j++)
                {
                    var col = m2.GetColumn(j);
                    var value = MatrixHelper.MultiplyRowByColumn(row, col);
                    result.Set(j, i, value);
                }
            }

            return result;
        }

        /// <summary>
        /// Calculates the determinant of the matrix.
        /// </summary>
        /// <returns></returns>
        private static double GetDeterminant2X2(Matrix<T> matrix)
        {
            return (((dynamic)matrix.Get(0, 0) * (dynamic)matrix.Get(1, 1)) - ((dynamic)matrix.Get(1, 0) * (dynamic)matrix.Get(0, 1)));
        }

        public double GetDeterminant()
        {
            if (typeof(T) != typeof(int) && typeof(T) != typeof(double))
            {
                throw new InvalidOperationException("T must be int or double to calculate the deteminant");
            }

            if (!IsSquare)
            {
                throw new InvalidOperationException("Matrix must be a square");
            }

            return GetDeterminantRecursive(this);
        }

        public static double GetDeterminantRecursive(Matrix<T> matrix)
        {
            if (matrix.Columns == 2) // Base case
            {
                return GetDeterminant2X2(matrix);
            }

            var result = 0;
            bool plus = true;
            //for (int j = 0; j < matrix.Rows; j++)
            //{
                for (int i = 0; i < matrix.Columns; i++)
                {
                    if (plus)
                    {
                        result += (dynamic) matrix.Get(i, 0)*
                                  GetDeterminantRecursive(matrix.GetSubMatrixExclusive(matrix, 0, i));
                    }
                    else
                    {
                        result -= (dynamic) matrix.Get(i, 0)*
                                  GetDeterminantRecursive(matrix.GetSubMatrixExclusive(matrix, 0, i));
                    }

                    plus = !plus;
                } 
            //}

            return result;
        }

        private Matrix<T> GetSubMatrixExclusive(Matrix<T> matrix, int y, int x)
        {
            var result = new Matrix<T>(matrix.Columns - 1, matrix.Rows - 1);

            for (int i = 0; i < matrix.Rows; i++) // Rows
            {
                for (int j = 0; j < matrix.Columns; j++) // Columns
                {
                    if (i != y && j != x)
                    {
                        var value = matrix.Get(j, i);
                        if (i < y && j < x)
                        {
                            result.Set(j, i, value);
                        }
                        else if (i >= y && j < x)
                        {
                            result.Set(j, i - 1, value);
                        }
                        else if (i < y && j >= x)
                        {
                            result.Set(j - 1, i, value);
                        }
                        else
                        {
                            result.Set(j - 1, i - 1, value);
                        }
                    }
                }
            }

            return result;
        }


        /// <summary>
        /// TODO
        /// </summary>
        /// <returns></returns>
        public static Matrix<T> GetInverse()
        {
            // Algorithm
            /*
             * Create the Matrix of Minors
             * Convert the Matrix of Minors to the Matrix of Cofactors
             * */
            throw new NotImplementedException();
        }



        #endregion

        #region [Searching]

        /// <summary>
        /// Determine if provided predicate matches any element in the matrix.
        /// </summary>
        /// <param name="match">Predicate to use for searching</param>
        /// <returns>True if the predicate matches any element; false if not</returns>
        public bool Exists(Predicate<T> match)
        {
            for (var i = 0; i < Rows; i++)
            {
                for (var j = 0; j < Columns; j++)
                {
                    if (match(Get(j, i)))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Determine if the provided value exists in the matrix 
        /// </summary>
        /// <param name="value">Value of type T</param>
        /// <returns>True if the value exists; false if not</returns>
        public bool Exists(T value)
        {
            return Exists(x => x.Equals(value));
        }

        /// <summary>
        /// Determines if the provided value exists in the row
        /// </summary>
        /// <param name="rowNumber">Row number for the row to search in</param>
        /// <param name="value">Value to search for</param>
        /// <returns>True if value is present in row; false if not</returns>
        public bool ExistsInRow(int rowNumber, T value)
        {
            return MatrixHelper.ExistsInArray(GetRow(rowNumber), value);
        }

        /// <summary>
        /// Determines if the provided value exists in the column
        /// </summary>
        /// <param name="columnNumber">Column number for the row to search in</param>
        /// <param name="value">Value to search for</param>
        /// <returns>True if value is present in column; false if not</returns>
        public bool ExistsInColumn(int columnNumber, T value)
        {
            return MatrixHelper.ExistsInArray(GetColumn(columnNumber), value);
        }

        /// <summary>
        /// Returns an array of columns that contains elements that meets the condition of the predicate
        /// </summary>
        /// <param name="match">Predicate condition to use to find columns</param>
        /// <returns>An array of columns</returns>
        public T[][] FindColumnsWithCriteria(Predicate<T> match)
        {
            return _matrix.Where(col => MatrixHelper.ExistsInArray(col, match)).ToArray();
        }

        /// <summary>
        /// Returns an array of rows that contains elements that meets the condition of the predicate
        /// </summary>
        /// <param name="match">Predicate condition to use to find rows</param>
        /// <returns>An array of rows</returns>
        public T[][] FindRowsWithCriteria(Predicate<T> match)
        {
            var result = new List<T[]>();

            for (var i = 0; i < Rows; i++)
            {
                var row = GetRow(i);
                if (MatrixHelper.ExistsInArray(row, match))
                {
                    result.Add(row);
                }
            }

            return result.ToArray();
        }

        #endregion

        #region [Overrides]
        /// <summary>
        /// Returns a string representation of the current matrix
        /// </summary>
        /// <returns>String representation of the matrix</returns>
        public override string ToString()
        {
            var builder = new StringBuilder("");
            for (var i = 0; i < Rows; i++)
            {
                for (var j = 0; j < Columns; j++)
                {
                    if (j < Columns - 1)
                    {
                        builder.Append(Get(j, i) + " ");
                    }
                    else
                    {
                        builder.Append(Get(j, i));
                    }
                }
                builder.Append("\n");
            }
            return builder.ToString().TrimEnd('\n');
        }

        /// <summary>
        /// Returns a List representation of the matrix
        /// </summary>
        /// <returns>List representation of the matrix</returns>
        public List<T> ToList()
        {
            var result = new List<T>();
            for (var i = 0; i < Rows; i++)
            {
                for (var j = 0; j < Columns; j++)
                {
                    result.Add(Get(j, i));
                }
            }
            return result;
        }
        #endregion

        #region [Private helper functions]

        private void SetMatrix(Matrix<T> result)
        {
            DataStructureManager.Set(this, result);
            Rows = result.Rows;
            Columns = result.Columns;
        }

        #endregion

        #region [Data structure manager]
        /// <summary>
        /// Encapsulets the Set() and Get() methods for the internal data structure
        /// </summary>
        static class DataStructureManager
        {
            /// <summary>
            /// Sets the internal datastructure of target from source
            /// </summary>
            /// <param name="target">Modify this matrix internal structure</param>
            /// <param name="source">Use this matrix internal structure</param>
            public static void Set(Matrix<T> target, Matrix<T> source)
            {
                Set(target, source._matrix);
            }

            /// <summary>
            /// Get the internal data structure of the provided matrix
            /// </summary>
            /// <param name="matrix">Matrix to extract structure from</param>
            /// <returns>Internal data structure</returns>
            // ReSharper disable UnusedMember.Local
            public static T[][] Get(Matrix<T> matrix)
            // ReSharper restore UnusedMember.Local
            {
                return matrix._matrix;
            }

            private static void Set(Matrix<T> matrix, T[][] structure)
            {
                matrix._matrix = structure;
            }
        }
        #endregion

        #region [Sudoku methods - TO BE MOVED]

        private List<int> CalculateCandidates(IEnumerable<List<int>> sources)
        {
            var allNoCandidates = new SortedSet<int>();
            foreach (var value in sources.SelectMany(sourceList => sourceList))
            {
                allNoCandidates.Add(value);
            }
            return new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9 }.Except(allNoCandidates).ToList();
        }

        private void CreateCandidatesMatrix()
        {
            var sMAtrix = new SudokuMatrix(9); // The init matrix should contain @Value and @0

            for (var i = 0; i < sMAtrix.Rows; i++)
            {
                for (var j = 0; j < sMAtrix.Columns; j++)
                {
                    var currentCell = sMAtrix.Get(j, i);
                    if (!currentCell.ValueFound && currentCell.Value == 0) // No real value found and current value is 0/undef
                    {
                        var row = sMAtrix.GetRow(i);
                        var rowList = ConvertToNonCandidateList(row, currentCell.Value);

                        var col = sMAtrix.GetColumn(i);
                        var colList = ConvertToNonCandidateList(col, currentCell.Value);

                        var subMatrix = sMAtrix.GetSubMatrixBounded(j, i, 3, 3);
                        var subMatrixList = Flatten(subMatrix);

                        currentCell.Candidates =
                            CalculateCandidates(new BindingList<List<int>>() {rowList, colList, subMatrixList});
                    }
                }
            }

        }

        public List<int> Flatten(Matrix<SudokuCell> m)
        {
            return (m.ToList().Where(sudokuCell => sudokuCell.Value != 0).Select(sudokuCell => sudokuCell.Value)).ToList();
        }

        private List<int> ConvertToNonCandidateList(SudokuCell[] cellArray, int currentValue)
        {
            return (cellArray.Where(sudokuCell => sudokuCell.Value != 0 && sudokuCell.Value != currentValue).Select(sudokuCell => sudokuCell.Value)).ToList();
        }

        public class SudokuMatrix : Matrix<SudokuCell>
        {
            public SudokuMatrix(int columns, int rows) : base(columns, rows)
            {
            }

            public SudokuMatrix(int size) : base(size)
            {
                Initilize(new Matrix<T>.SudokuCell{Candidates = new List<int>()});
            }
        }

        public class SudokuCell : IConvertible
        {
            public List<int> Candidates { set; get; }

            public int Value
            {
                get { return Candidates.Any() ? Candidates.First() : 0; }
            }

            public bool ValueFound
            {
                get; set; 
            }

            public TypeCode GetTypeCode()
            {
                throw new NotImplementedException();
            }

            public bool ToBoolean(IFormatProvider provider)
            {
                throw new NotImplementedException();
            }

            public char ToChar(IFormatProvider provider)
            {
                throw new NotImplementedException();
            }

            public sbyte ToSByte(IFormatProvider provider)
            {
                throw new NotImplementedException();
            }

            public byte ToByte(IFormatProvider provider)
            {
                throw new NotImplementedException();
            }

            public short ToInt16(IFormatProvider provider)
            {
                throw new NotImplementedException();
            }

            public ushort ToUInt16(IFormatProvider provider)
            {
                throw new NotImplementedException();
            }

            public int ToInt32(IFormatProvider provider)
            {
                throw new NotImplementedException();
            }

            public uint ToUInt32(IFormatProvider provider)
            {
                throw new NotImplementedException();
            }

            public long ToInt64(IFormatProvider provider)
            {
                throw new NotImplementedException();
            }

            public ulong ToUInt64(IFormatProvider provider)
            {
                throw new NotImplementedException();
            }

            public float ToSingle(IFormatProvider provider)
            {
                throw new NotImplementedException();
            }

            public double ToDouble(IFormatProvider provider)
            {
                throw new NotImplementedException();
            }

            public decimal ToDecimal(IFormatProvider provider)
            {
                throw new NotImplementedException();
            }

            public DateTime ToDateTime(IFormatProvider provider)
            {
                throw new NotImplementedException();
            }

            public string ToString(IFormatProvider provider)
            {
                throw new NotImplementedException();
            }

            public object ToType(Type conversionType, IFormatProvider provider)
            {
                throw new NotImplementedException();
            }
        }
      
        #endregion
    }

    /// <summary>
    /// Static class containing helper methods not connected to any object
    /// </summary>
    public static class Matrix
    {
        public enum Degrees
        {
            /// <summary>
            /// 90 degrees
            /// </summary>
            Clockwise90,
            /// <summary>
            /// 180 degreees
            /// </summary>
            Clockwise180,
            /// <summary>
            /// 270 degrees
            /// </summary>
            Clockwise270
        }

        /// <summary>
        /// Delegate used as function pointer to enable use of caller specific filtering
        /// </summary>
        /// <typeparam name="T">Type T</typeparam>
        /// <param name="x">First operand, of type T</param>
        /// <param name="y">Second operand, of type T</param>
        /// <returns></returns>
        internal delegate T Op<out T>(dynamic x, dynamic y) where T : IConvertible;

        /// <summary>
        /// Concatenates the provided matrices vertically. Matrices must have the same number of columns.
        /// </summary>
        /// <param name="matrices">Array of matrices</param>
        /// <returns>A new matrix with the combined number of rows for the provided matrices</returns>
        public static Matrix<T> ConcatenateVertically<T>(Matrix<T>[] matrices) where T : IConvertible
        {
            var height = matrices.Sum(subMatrix => subMatrix.Rows);
            var result = new Matrix<T>(matrices[0].Columns, height);

            var y = 0;
            foreach (var matrix in matrices)
            {
                for (var i = 0; i < matrix.Rows; i++)
                {
                    for (var j = 0; j < matrix.Columns; j++)
                    {
                        result.Set(j, i + y, matrix.Get(j, i));
                    }
                }

                y += matrix.Rows;
            }

            return result;
        }

        /// <summary>
        /// Concatenates the provided matrices horizontally. Matrices must have the same number of rows.
        /// </summary>
        /// <param name="matrices">Array of matrices</param>
        /// <returns>A new matrix with the combined number of columns for the provided matrices</returns>
        public static Matrix<T> ConcatenateHorizontally<T>(Matrix<T>[] matrices) where T : IConvertible
        {
            var width = matrices.Sum(subMatrix => subMatrix.Columns);
            var result = new Matrix<T>(width, matrices[0].Rows);

            var x = 0;
            foreach (var matrix in matrices)
            {
                for (var i = 0; i < matrix.Rows; i++)
                {
                    for (var j = 0; j < matrix.Columns; j++)
                    {
                        result.Set(j + x, i, matrix.Get(j, i));
                    }
                }

                x += matrix.Columns;
            }

            return result;
        }

        internal static Matrix<T> DoOperation<T>(Matrix<T> m1, Matrix<T> m2, Op<T> op) where T : IConvertible
        {
            if (m1.Columns != m2.Columns || m1.Rows != m2.Rows)
            {
                throw new ArgumentException("Provided matrices must be of the same size");
            }

            if (typeof(T) == typeof(bool))
            {
                throw new InvalidOperationException("The operator cannot be used with boolean matrices");
            }

            var result = new Matrix<T>(m1.Columns, m2.Rows);
            for (var i = 0; i < result.Rows; i++)
            {
                for (var j = 0; j < result.Columns; j++)
                {
                    dynamic m1Val = m1.Get(j, i);
                    dynamic m2Val = m2.Get(j, i);
                    var valResult = op(m1Val, m2Val);
                    result.Set(j, i, valResult);
                }
            }

            return result;
        }

        internal static Matrix<T> Rotate<T>(Matrix<T> matrix) where T : IConvertible
        {
            var newMatrix = new Matrix<T>(0);

            for (var i = 0; i < matrix.Columns; i++)
            {
                var col = matrix.GetColumn(i).Reverse().ToArray();
                if (i == 0)
                {
                    newMatrix.SetupFromRow(col);
                }
                else
                {
                    newMatrix.InsertRow(col, matrix.Rows - 1);
                }
            }

            return newMatrix;
        }
    }
}
