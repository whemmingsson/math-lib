﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Matrix.Base;

namespace Matrix.Variants
{
    /// <summary>
    /// Class for modeling a transformable matrix with special transformation and search methods
    /// </summary>
    /// <typeparam name="T">IConvertible type</typeparam>
    public class StructMatrix<T> : MatrixBase<T> where T : IConvertible
    {
    }
}
