﻿using System;

namespace Matrix
{
    public class MatrixFileLoader<T> : IMatrixLoader<T> where T : IConvertible
    {
        public Matrix<T> Load(string path)
        {
            var result = new Matrix<T>(0);

            var helper = new FileHelper(path);
            var fileContent = helper.Read();

            if (fileContent == string.Empty)
            {
                throw new Exception("File does not exist or is empty");
            }

            var fileRows = fileContent.Split("\r\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            var defenitonRow = fileRows[0];
            var sizeDefenitions = defenitonRow.Split(';');

            int rows;
            int cols;
            try
            {
                rows = int.Parse(sizeDefenitions[0]);
                cols = int.Parse(sizeDefenitions[1]);
                result = new Matrix<T>(cols, rows);
            }
            catch
            {
                throw new Exception("Could not read size defenitions from file");
            }

            if (rows != fileRows.Length -1)
            {
                throw new Exception("Size defenitions does not match actual matrix size");
            }
            if (cols != fileRows[1].Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries).Length)
            {
                throw new Exception("Size defenitions does not match actual matrix size");
            }

            for (int i = 1; i < rows; i++)
            {
                var values = fileRows[i].Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                var j = 0;
                foreach (var value in values)
                {
                    var typedValue = (T)Convert.ChangeType(value, typeof(T));
                    result.Set(j, i-1, typedValue);
                    j++;
                }
            }

            return result;
        }
    }
}
