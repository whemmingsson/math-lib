﻿using System;

namespace Matrix
{
    public interface IMatrixLoader<T> where T : IConvertible
    {
        Matrix<T> Load(string path);
    }
}