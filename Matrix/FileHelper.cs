﻿using System;
using System.IO;

namespace Matrix
{
    public class FileHelper
    {
        private string _fileName;

        public string FileName
        {
            get { return _fileName; }
            set { _fileName = value; }
        }

        public FileHelper(){}

        public FileHelper(string fileName)
        {
            _fileName = fileName;
        }

        public string Read()
        {
            try
            {              
                return File.ReadAllText(_fileName);
            }
            catch (Exception)
            {
                return "";
            }
        }
    }
}
