﻿using System;

namespace Matrix
{
    /// <summary>
    /// Static helper class for functions not needed to be defined in the Matrix class
    /// </summary>
    internal static class MatrixHelper
    {
        internal static bool ExistsInArray<T>(T[] array, T value) where T : IConvertible
        {
            return ExistsInArray(array, obj => obj.Equals(value));
        }

        internal static bool ExistsInArray<T>(T[] array, Predicate<T> match) where T : IConvertible
        {
            return Array.Exists(array, match);
        }

        internal static double MultiplyRowByColumn<T>(T[] row, T[] column)
        {
            double result = 0;
            for (int i = 0; i < row.Length; i++)
            {
                dynamic rowValue = row[i];
                dynamic colValue = column[i];
                result += (rowValue * colValue);
            }

            return result;
        }
    }
}
