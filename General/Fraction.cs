﻿using System;

namespace General
{
    /// <summary>
    /// Models a fraction for exact calculations
    /// </summary>
    public class Fraction
    {
        private double _enumerator;
        private double _denominator;

        public double Enumerator { get { return _enumerator; } set { _enumerator = value; } }

        public double Denominator
        {
            get { return _denominator; }
            set
            {
                if (value.Equals(0))
                {
                    throw new DivideByZeroException("The denominator cannot be zero");
                }

                _denominator = value;
            }
        }

        public double Value
        {
            get { return _enumerator/_denominator; }
        }

        public Fraction(double enumerator, double denominator)
        {
            Enumerator = enumerator;
            Denominator = denominator;
        }

        public Fraction() {}

        public static Fraction operator +(Fraction f1, Fraction f2)
        {
            if(!f1.Denominator.Equals(f2.Denominator))
            {
                throw new InvalidOperationException("To add fractions the denominators must be the same");
            }

            return new Fraction(f1.Enumerator + f2.Enumerator, f1.Denominator);
        }

        public static Fraction operator -(Fraction f1, Fraction f2)
        {
            if (!f1.Denominator.Equals(f2.Denominator))
            {
                throw new InvalidOperationException("To substract fractions the denominators must be the same");
            }

            return new Fraction(f1.Enumerator - f2.Enumerator, f1.Denominator);
        }

        public static Fraction operator *(Fraction f1, Fraction f2)
        {
            return new Fraction(f1.Enumerator * f2.Enumerator, f1.Denominator * f2.Denominator);
        }

        public static Fraction operator *(Fraction f1, double value)
        {
            return new Fraction(f1.Enumerator * value, f1.Denominator * value);
        }

        public static Fraction operator *(double value, Fraction f1)
        {
            return f1*value;
        }
    }
}
