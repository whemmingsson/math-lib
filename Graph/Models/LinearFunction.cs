﻿namespace Graph.Models
{
    class LinearFunction : IFunction
    {
        public double K { set; get; }
        public double M { set; get; }

        public double Get(double value)
        {
            return K*value + M;
        }
    }
}
