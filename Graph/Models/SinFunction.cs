﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graph.Models
{
    // Basic wrapper for the Sin() function
    class SinFunction : IFunction
    {
        /// <summary>
        /// Returns the value of sin(value) where value is in radians
        /// </summary>
        /// <param name="value">Radians, ranging from 0 to 2*Math.PI</param>
        /// <returns>Sin-value of value</returns>
        public double Get(double value)
        {
            return Math.Sin(value);
        }
    }
}
