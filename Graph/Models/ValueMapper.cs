﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graph.Models
{
    /// <summary>
    /// Maps the mathimatical value to a on-screen representation of the value
    /// </summary>
    public class ValueMapper
    {
        public int ViewportWidth { set; get; }
        public int ViewportHeight { set; get; }

        public Point Map(double value, IFunction function)
        {
            var x = MapX(value);
            var y = MapY(function.Get(value));
            return new Point((int)x, (int)y);
        }

        private double MapY(double valueY)
        {
            return (ViewportHeight - valueY) * ViewportWidth / ViewportHeight * 2;
        }

        private double MapX(double value)
        {
            return value;
        }
    }
}
