﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graph.Models
{
    class Graph : List<Point>
    {
        public Pen Pen { set; get; }

        public void Draw(Graphics graphics)
        {
            graphics.DrawCurve(Pen, this.ToArray());
        }
    }
}
