﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Graph.Models;

namespace Graph
{
    public partial class Form1 : Form
    {
        private GraphHolder _graphs;
        private ValueMapper _mapper;

        public Form1()
        {
            InitializeComponent();
            drawingArea.Paint += new PaintEventHandler(Draw);
            _graphs = new GraphHolder();
           _mapper = new ValueMapper(){ViewportWidth = drawingArea.Width, ViewportHeight = drawingArea.Height};

            var linGaph = new Models.Graph() {Pen = new Pen(Color.DodgerBlue)}; // Empty graph
            var linFunc = new LinearFunction {M = 0, K = 1};

            for (var x = 0; x < _mapper.ViewportWidth; x++)
            {
                var point = _mapper.Map(x, linFunc);
                linGaph.Add(point);
            }

            _graphs.Add(linGaph);
        }

        protected Point Origo
        {
            get
            {
                return new Point(drawingArea.Width / 2, drawingArea.Height / 2);
            }
        }

        protected int H
        {
            get { return drawingArea.Height; }
        }

         protected int W
        {
            get { return drawingArea.Width; }
        }

        private void Draw(object sender, PaintEventArgs e)
        {
            var graphics = e.Graphics;
            graphics.Clear(Color.FromArgb(255, 40,40,40));

            DrawAxis(graphics);
            //DrawCurve(graphics, new Pen(Color.Red));

            _mapper.ViewportWidth = drawingArea.Width;
            _mapper.ViewportHeight = drawingArea.Height;

            UpdateModel();

            foreach (var graph in _graphs)
            {
                graph.Draw(graphics);
            }

            //drawingArea.Invalidate();
        }

        private void UpdateModel()
        {
            _graphs.Clear();
            var linGraph = new Models.Graph() { Pen = new Pen(Color.DodgerBlue) }; // Empty graph
            var linFunc = new LinearFunction { M = 0, K = 1 };

            for (var x = 0; x < _mapper.ViewportWidth; x++)
            {
                var point = _mapper.Map(x, linFunc);
                linGraph.Add(point);
            }

            _graphs.Add(linGraph);
        }

        private void Form1_Load(object sender, EventArgs e)
        {         
                   
        }

        private void drawBtn_Click(object sender, EventArgs e)
        {
            //DrawCurve(drawingArea.CreateGraphics());
        }


        private void Form1_Resize(object sender, EventArgs e)
        {
            drawingArea.Invalidate();
        }


        private void DrawAxis(Graphics g)
        {           
            int length = 5; // 10 px
            var pen = new Pen(Color.White, 1);

            // Draw axes
            g.DrawLine(pen, drawingArea.Width / 2, 0, drawingArea.Width / 2, drawingArea.Height);
            g.DrawLine(pen, 0, drawingArea.Height/2, drawingArea.Width, drawingArea.Height/2 );

            int w = drawingArea.Width;
            int h = drawingArea.Height;

            // Draw lines for the pi values
            g.DrawLine(pen, Origo.X - length, Origo.Y - h/4, Origo.X + length, Origo.Y - h/4);
            g.DrawLine(pen, Origo.X - length, Origo.Y + h / 4, Origo.X + length, Origo.Y + h / 4);

            g.DrawLine(pen, Origo.X - w/4, Origo.Y - length, Origo.X - w/4, Origo.Y + length);
            g.DrawLine(pen, Origo.X + w / 4, Origo.Y - length, Origo.X + w / 4, Origo.Y + length);       
        }

        private void DrawCurve(Graphics g, Pen pen)
        {
           var points = new List<Point>();

           for (int i = 0; i < 360; i++)
           {
              points.Add(CreateSinusPoint(i, Origo));
           }

            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            g.DrawCurve(pen, points.ToArray());
        }

        private Point CreateSinusPoint(int deg, Point startingPoint)
        {
            var rad = DegToRad(deg);
            var y = -(Math.Sin(rad) * H / 4);
            var x = (deg / 360.0)* W / 2;
            return new Point((int)x + startingPoint.X, (int)y + startingPoint.Y);
        }

        private double DegToRad(int deg)
        {
            return (deg * Math.PI / 180.0);
        }

        private void drawingArea_MouseClick(object sender, MouseEventArgs e)
        {
            //var g = drawingArea.CreateGraphics();
            //var blue = new Pen(Color.Blue);

            //DrawCurve(g, blue );
            drawingArea.Invalidate();
          
        }      
    }
}
